﻿using System;
using System.IO;
using NLog;
using Raise.Workbench.Service;
using Topshelf;
using Configuration = Raise.Workbench.Service.Configuration;

namespace Workbench {
    public class Program {
        // ReSharper disable once UnusedMember.Local
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger(typeof(Program));

        public static void Main(string[] args) {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            HostFactory.Run(x => {
                x.RunAsLocalSystem();
                x.StartAutomatically();

                x.SetDescription(Configuration.ServiceDescription);
                x.SetDisplayName(Configuration.ServiceDisplayName);
                x.SetServiceName(Configuration.ServiceName);

                x.Service(factory => {
                    QuartzService server = QuartzServiceFactory.CreateServer();
                    server.Initialize().GetAwaiter().GetResult();
                    return server;
                });
            });
            Console.ReadKey();
        }
    }
}
